package com.crmweb.controller;

import com.crmweb.services.ServiceMain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class HomeController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ServiceMain serviceMain;


	@Autowired
	private ApplicationContext applicationContext;


	@RequestMapping(value="/")
	public ModelAndView test(){
		logger.info("Main Call to /");
		Map<String, Object> datamap = new HashMap<String, Object>();
		datamap.put("date", serviceMain.getDate());
		datamap.put("baseUrl", applicationContext.getApplicationName());
		return new ModelAndView("home" ,datamap);
	}


	@RequestMapping(value="/user")
	public ModelAndView showUser(@RequestParam("id") String id){
		logger.info("Searching for user with id={}", id);
		Map<String, Object> datamap = new HashMap<String, Object>();
		datamap.put("date", serviceMain.getDate());
		datamap.put("userName", serviceMain.getUsername(id));
		return new ModelAndView("home" ,datamap);
	}


	@RequestMapping(value="/email/{user}")
	@ResponseBody
	public String PrintEmail(@PathVariable("user") String userName){
		logger.info("Searching for email of user={}", userName);
		return "Email="+serviceMain.getEmailForUser(userName);
	}


	// Total control - setup a model and return the view name yourself. Or
	// consider subclassing ExceptionHandlerExceptionResolver (see below).
	@ExceptionHandler(RuntimeException.class)
	public ModelAndView handleError(HttpServletRequest req, RuntimeException ex) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("exception", ex);
		mav.addObject("url", req.getRequestURL());
		mav.setViewName("error");
		return mav;
	}
}
