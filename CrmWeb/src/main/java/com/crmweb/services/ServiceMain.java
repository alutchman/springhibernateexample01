package com.crmweb.services;


import com.domain.dpa.dao.DaoContact;
import com.domain.dpa.tables.Contact;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceMain {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DaoContact<Contact> daoContact;

    public String getUsername(String id) {
        Contact contact = daoContact.fetchById(Long.valueOf(id));

        if (contact == null) {
            logger.error("User not found for id:"+id);
            throw new RuntimeException("User not found for id:"+id);
        }
        return contact.getUserName();
    }

    public String getEmailForUser(String userName) {
        Contact contact = daoContact.findContact(userName);
        if (contact == null) {
            logger.error("User not found for userName:"+userName);
            throw new RuntimeException("User not found for username:"+userName);
        }
        return contact.getEmail();
    }



    public String getDate() {
        DateTime dt = new DateTime();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss");
        String str = fmt.print(dt);

        return str;
    }
}
