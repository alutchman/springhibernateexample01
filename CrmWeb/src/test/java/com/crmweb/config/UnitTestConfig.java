package com.crmweb.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(
        basePackages = {"com.crmweb"}
)
public class UnitTestConfig {

}
