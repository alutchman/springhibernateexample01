package com.crmweb.services;

import com.domain.dpa.dao.DaoContact;
import com.domain.dpa.tables.Contact;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ServiceMainTest {
    private static final String EMAIL_OK = "gogo@ict.com" ;

    @Mock
    public DaoContact<Contact> daoContact;

    @Mock
    public Contact contact;

    @InjectMocks
    public ServiceMain serviceMain;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    @Before
    public void setup(){
        when(contact.getEmail()).thenReturn(EMAIL_OK);
    }

    @Test
    public void happyFlowGetUserName(){
        String userStr = "1";
        String dummyUser = "Dummy";
        when(contact.getUserName()).thenReturn(dummyUser);
        when(daoContact.fetchById(Long.valueOf(userStr))).thenReturn(contact);
        String userResult = serviceMain.getUsername(userStr);
        assertEquals(dummyUser, userResult);
    }

    @Test
    public void userNotFound(){
        String userStr = "2";
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("User not found for id:"+userStr);

        when(daoContact.fetchById(Long.valueOf(userStr))).thenReturn(null);
        serviceMain.getUsername(userStr);
    }

    @Test
    public void invalidArgument(){
        String userStr = "fdsfdsf";
        expectedException.expect(NumberFormatException.class);
        expectedException.expectMessage("For input string: \""+  userStr + "\"" );
        serviceMain.getUsername(userStr);
    }

    @Test
    public void displayDate(){
        String currentDate = serviceMain.getDate();
        assertNotNull(currentDate);
    }


    @Test
    public void testemailUserFound(){
        String userStr = "userOK";
        when( daoContact.findContact(userStr)).thenReturn(contact);
        String email = serviceMain.getEmailForUser(userStr);
        assertEquals(EMAIL_OK, email);
    }


    @Test
    public void testemailUserNotFound(){
        String userStr = "undefUser";
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("User not found for username:"+  userStr  );
        when( daoContact.findContact(userStr)).thenReturn(null);
        serviceMain.getEmailForUser(userStr);
    }

}