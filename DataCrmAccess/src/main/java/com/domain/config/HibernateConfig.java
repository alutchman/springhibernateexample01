package com.domain.config;


import com.jndi.env.JndiTemplateDataBase;
import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@ComponentScan(
        basePackages = { "com.domain.dpa" }
)
@EnableTransactionManagement
public class HibernateConfig {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ApplicationContext applicationContext;


    private final SimpleNamingContextBuilder contextBuilder = new SimpleNamingContextBuilder();


    @Bean
    public JndiTemplateDataBase fetchJndiTemplate(){
        JndiTemplateDataBase template = new JndiTemplateDataBase();
        return template;
    }


    @Autowired
    @Bean
    public DataSource getDataSource(JndiTemplateDataBase jndiTemplateDataBase)  {
        BasicDataSource ds = new BasicDataSource();

        ds.setDriverClassName(jndiTemplateDataBase.getProperty("jdbc.driverClassName"));
        ds.setUrl(jndiTemplateDataBase.getProperty("jdbc.url"));
        ds.setUsername(jndiTemplateDataBase.getProperty("jdbc.username"));
        ds.setPassword(jndiTemplateDataBase.getProperty("jdbc.password"));

        logger.info("Loaded Datasource for url {}", jndiTemplateDataBase.getProperty("jdbc.url"));

        return ds;
    }



    @Bean
    @Autowired(required=true)
    public JdbcTemplate createJdbCTemplate(DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }



    @Bean
    @Autowired(required=true)
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, JndiTemplateDataBase template) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan(new String[] {"com.domain.dpa.tables" });

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(template.getEnvironment());

        return em;
    }


    @Bean
    @Autowired(required=true)
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);

        return transactionManager;
    }


}
