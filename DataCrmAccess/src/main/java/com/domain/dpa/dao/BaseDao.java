package com.domain.dpa.dao;

import com.domain.dpa.entity.BaseEntity;
import org.hibernate.Session;

import javax.persistence.Query;
import java.util.List;

public interface BaseDao<T extends BaseEntity> {

    public Session getSession();
    public List<T> fetchAll() ;
    public Query createQuery();
    public Query createQuery(String conditions);
    public boolean add(T model) ;
    public void delete(T model);

}
