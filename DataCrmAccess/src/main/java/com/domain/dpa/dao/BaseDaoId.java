package com.domain.dpa.dao;

import com.domain.dpa.entity.BaseEntityId;

public interface BaseDaoId<T extends BaseEntityId>  extends BaseDao<T> {
    public T fetchById(long id);
}
