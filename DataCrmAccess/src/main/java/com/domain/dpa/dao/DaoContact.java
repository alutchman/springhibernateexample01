package com.domain.dpa.dao;

import com.domain.dpa.entity.BaseEntityId;
import com.domain.dpa.tables.*;

public interface DaoContact<T extends BaseEntityId> extends BaseDaoId<T> {

    Contact findContact(String  username );


}
