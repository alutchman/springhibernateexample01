package com.domain.dpa.impl;

import com.domain.dpa.dao.BaseDaoId;
import com.domain.dpa.entity.BaseEntityId;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.MappedSuperclass;


@MappedSuperclass
@Transactional(propagation = Propagation.REQUIRED)
public class BaseDaoIdImpl<T extends BaseEntityId>  extends BaseDaoImpl<T> implements BaseDaoId<T>{
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public T fetchById(long id)  {
        return (T) entityManager.find(persistentClass, id);
    }

}
