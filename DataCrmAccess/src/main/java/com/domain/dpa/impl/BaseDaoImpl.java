package com.domain.dpa.impl;


import com.domain.dpa.dao.BaseDao;
import com.domain.dpa.entity.BaseEntity;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.lang.reflect.ParameterizedType;
import java.util.List;

@MappedSuperclass
@Transactional
public abstract class BaseDaoImpl<T extends BaseEntity> implements BaseDao<T> {

    protected final Class<T> persistentClass = (Class<T>) ((ParameterizedType) getClass()
            .getGenericSuperclass()).getActualTypeArguments()[0];

    protected final String selectAll   = "FROM "+persistentClass.getSimpleName();
    protected final String selectWhere = selectAll + " WHERE %s";


    protected final Table table = persistentClass.getAnnotation(Table.class);

    @Autowired
    protected JdbcTemplate jdbcTemplate;


    @PersistenceContext
    protected EntityManager entityManager;


    public Session getSession(){
       return entityManager.unwrap(Session.class);
    }


    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<T> fetchAll() {
        return createQuery().getResultList();
    }


    public Query createQuery(){
       return entityManager.createQuery(selectAll);
    }


    public Query createQuery(String conditions){
        return entityManager.createQuery(String.format(selectWhere, conditions));
    }


    /**
     *
     * @param model
     * @return
     */
    public boolean add(T model) {
        entityManager.persist(model);
        return true;
    }

    public void delete(T model) {
        entityManager.remove(model);
    }

}
