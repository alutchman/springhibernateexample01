package com.domain.dpa.impl;



import com.domain.dpa.dao.DaoContact;
import com.domain.dpa.tables.Contact;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;


@Component
public class DaoContactImpl extends BaseDaoIdImpl<Contact> implements DaoContact<Contact> {

	@SuppressWarnings("unchecked")
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Contact findContact(String  username ) {
		Query query0 = createQuery("userName=:username");      ;
		query0.setParameter("username",username);
		List<Contact> items = (List<Contact>) query0.getResultList();
		return items.size() == 0 ? null : items.get(0);
	}
}
