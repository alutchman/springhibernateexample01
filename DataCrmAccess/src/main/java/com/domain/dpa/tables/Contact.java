package com.domain.dpa.tables;

import com.domain.dpa.entity.BaseEntityId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "contact")
public class Contact extends BaseEntityId {
    private static final long serialVersionUID = -3950272868279478120L;

    @Column( name = "username",nullable = false, length = 20, unique = true)
    private String userName;


    @Column(name = "accesCode", nullable = false, length = 20)
    public String password;

    @Column(name = "email", nullable = true, length = 100)
    private String email;

    @Column(name = "gsm", nullable = true, length = 25)
    private String gsm;



    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public String getGsm() {
        return gsm;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setGsm(String gsm) {
        this.gsm = gsm;
    }

}
