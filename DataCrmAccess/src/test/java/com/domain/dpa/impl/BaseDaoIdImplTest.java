package com.domain.dpa.impl;

import com.domain.config.HibernateConfig;
import com.domain.dpa.tables.Contact;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class})
public class BaseDaoIdImplTest extends BaseDaoIdImpl<Contact> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    private long initialIdFirstEntry = 2;


    @Transactional
    @Before
    public void setup(){
        jdbcTemplate.execute("INSERT INTO contact (username,email,gsm, accesCode) VALUES('alutchman','alfa@dpa.com','0629317687','xxxxx') ");
        jdbcTemplate.execute("INSERT INTO contact (username,email,gsm, accesCode) VALUES('bbiharie','baby@dpa.com','0629317688','xxxxx') ");

        // String sql = "SELECT id FROM contact WHERE username = ?";
        // Contact contact = jdbcTemplate.queryForObject(sql, Contact.class, "alutchman");
        // initialIdFirstEntry = contact.getId();
    }

    @Transactional
    @After
    public void breakDown(){
        jdbcTemplate.execute("TRUNCATE TABLE contact ");
    }




    @Test
    public void testFetchAll()  {
        List<Contact> contacts = fetchAll();
        assertEquals(2, contacts.size());
    }

    @Transactional
    @Test
    public void testAddHappyFlow()  {
        Contact contact = new Contact();
        contact.setUserName("alutchman2");
        contact.setEmail("a@b.com");
        contact.setPassword("xxxxx");
        add(contact);

        List<Contact> contacts = fetchAll();
        assertEquals(3, contacts.size());
    }

    @Transactional
    @Test
    public void testDelete() {
        List<Contact> contacts = fetchAll();
        int initialSize = contacts.size();
        System.out.println("Old Size = " + initialSize);
        Contact contact  = contacts.get(0);
        long initialId = contact.getId();
        assertNotNull(contact);
        delete(contact);
        Contact contactAfterDelete  = this.fetchById(initialId);
        assertNull(contactAfterDelete);
        contacts = fetchAll();
        System.out.println("New Size = " + contacts.size());
        assertEquals(initialSize-1, contacts.size());
    }


    @Transactional
    @Test
    public void testDeleteUnhappyFlow() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(startsWith("Removing a detached instance com.domain.dpa.tables.Contact"));

        Contact contact = new Contact();
        contact.setUserName("alutchman4");
        contact.setEmail("c@c.com");
        contact.setPassword("yyyyy");
        contact.setId(20L);
        delete(contact);
    }

    @Test
    public void testMerge()  {
        String newEmail = "newEamil@dpa.nl";
        List<Contact> contacts = fetchAll();
        Contact contact  = contacts.get(0);
        String orgEmail = contact.getEmail();
        contact.setEmail(newEmail);
        //this.merge(contact);

        assertEquals(newEmail, contact.getEmail());
    }

    @Test
    public void testFetchById() throws Exception {

    }
}