package com.domain.dpa.impl;

import com.domain.config.HibernateConfig;
import com.domain.dpa.dao.DaoContact;
import com.domain.dpa.tables.Contact;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class})
public class DaoContactImplTest {

    @Autowired
    private DaoContact<Contact> daoContact;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testFindContact()  {

    }

    @Transactional
    @Before
    public void setup(){
        jdbcTemplate.execute("INSERT INTO contact (username,email,gsm, accesCode) VALUES('alutchman','alfa@dpa.com','0629317687','xxxxx') ");
        jdbcTemplate.execute("INSERT INTO contact (username,email,gsm, accesCode) VALUES('bbiharie','baby@dpa.com','0629317688','xxxxx') ");

    }

    @Transactional
    @After
    public void breakDown(){
        jdbcTemplate.execute("TRUNCATE TABLE contact ");
    }


    @Test
    public void testHappyFlowFindContact(){
        Contact contact  = daoContact.findContact("alutchman");
        assertNotNull(contact);
    }



    @Transactional
    @Test
    public void testUnique(){
        expectedException.expect(PersistenceException.class);
        expectedException.expectMessage("could not execute statement");

        Contact contact = new Contact();
        contact.setUserName("alutchman");
        contact.setEmail("a@b.com");
        contact.setPassword("xxxxx");
        daoContact.add(contact);

    }


    @Transactional
    @Test
    public void testUpdate(){
        String newEmail = "newEamil@dpa.nl";
        Contact contact  = daoContact.findContact("alutchman");
        contact.setEmail(newEmail);

        Contact contact2  = daoContact.findContact("alutchman");
        assertEquals(newEmail, contact2.getEmail());

    }

}