# README #

* Multi Module
* Spring Web MVC 4.3
* Hibernate 5.2
* JPA
* Annotation driven design.
* Spring Unit Test using H2 in memory database
* Logback used for logging
